# Дан список: [Ivan, Ivanou], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

f_i = ['Ivan', 'Ivanou']
gor = 'Minsk'
st = 'Belarus'

print("Привет, " + (' '.join(f_i)) + f"! Добро пожаловать в {gor} {st}")
