# Игра быки и коровы

import random

secret_num = ''.join(random.sample('1234567890', 4))
print(secret_num)

step = 1

while True:

    bull = 0
    cow = 0

    num = input('%d Attempt to guess four digits: ' % step)

    if secret_num == num:
        print('You won!')
        break

    for i in num:
        if num.count(i) > 1:
            print('Enter a 4-digit number with non-repeating digits')
            break

        if i in secret_num:
            if num.index(i) != secret_num.index(i):
                cow += 1
            elif num.index(i) == secret_num.index(i):
                bull += 1

    if cow != 1 != bull:
        print(f"{cow} cows, {bull} bulls")
    elif cow == 1 != bull:
        print(f"{cow} cow, {bull} bulls")
    elif cow != 1 == bull:
        print(f"{cow} cows, {bull} bull")
    else:
        print(f"{cow} cow, {bull} bull")
    step += 1
