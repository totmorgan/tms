def typed(typ_e):
    def decorator(func):
        def wrapper(*args):
            kor = []

            for i in args:
                if typ_e == 'str' and len(args):
                    kor.append(str(i))
                elif type(i) == int and len(args):
                    kor.append(int(i))
                elif type(i) == float and len(args):
                    kor.append(float(i))

            return func(*kor)
        return wrapper
    return decorator


@typed(typ_e='str')
def add(*args):

    print(''.join(args))


add("3", 5, 4, 'sfr', '343')
add(5, 5)
add('a', 'b')


@typed(typ_e='int')
def add(*args):

    return sum(args)


print(add(5, 6, 7))
print(add(0.1, 0.2, 0.4))
print(add(5, 6))
print(add(5, 6, 7, 8, 9, 10, 11, 12, 13, 14))
