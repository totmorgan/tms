arr = input('enter the names:')

st = arr.split()

if len(st) == 0:
    print('no one likes this')
elif len(st) == 1:
    name = st[0]
    print(f'{name} likes this')
elif len(st) == 2:
    name_1 = st[0]
    name_2 = st[1]
    print(f'{name_1} and {name_2} like this')
elif len(st) == 3:
    name_1 = st[0]
    name_2 = st[1]
    name_3 = st[2]
    print(f'{name_1}, {name_2} and {name_3} like this')
elif len(st) > 3:
    name_1 = st[0]
    name_2 = st[1]
    name_other = len(st) - 2
    print(f'{name_1}, {name_2} and {name_other} others like this')
