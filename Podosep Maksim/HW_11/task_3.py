import yaml
from pprint import pprint

file = 'd:/TMS/tms1_2/tms_1_2/Podosep Maksim/order.yaml'


def func_yaml(f1):
    with open(f1) as f:
        file_order = yaml.safe_load(f)

    pprint(file_order, indent=4)

    for k, v in file_order.items():
        if k == 'invoice':
            print('номер заказа - ', v)
        elif k == 'bill-to':
            pprint(f'адрес отправки- {v}')
        elif k == 'product':
            pprint(f'описание посылки -{v}')


func_yaml(file)
