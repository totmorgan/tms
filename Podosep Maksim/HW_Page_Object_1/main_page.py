from pages.base_page import BasePage
from locator.main_page_locator import MainPageLocator


class MainPage(BasePage):

    def open_login_page(self):
        sing_in_button = self.find_element(
            MainPageLocator.LOCATOR_SING_IN_BUTTON)
        sing_in_button.click()

    def click_cart(self):
        cart_button = self.find_element(
            MainPageLocator.LOCATOR_CART_BUTTON)
        cart_button.click()

    def click_catalog_women(self):
        catalog_button = self.find_element(
            MainPageLocator.LOCATOR_CATALOG_WOMEN)
        catalog_button.click()
