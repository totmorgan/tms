from pages.main_page import MainPage
from pages.contact_us_page import ContactUsPage
from pages.tool_bar_page import ToolBarPage


def test_open(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    tool_bar_page = ToolBarPage(browser)
    tool_bar_page.click_contact_us()
    contact_us_page = ContactUsPage(browser)
    contact_us_page.click_selector()
    contact_us_page.subject_enter()
    contact_us_page.email_enter()
    contact_us_page.order_enter()
    contact_us_page.message_enter()
    contact_us_page.submit_click()
    contact_us_page.sent_message()
