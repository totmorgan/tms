from pages.base_page import BasePage
from locator.tool_bar_locator import ToolBarPageLocator


class ToolBarPage(BasePage):

    def click_contact_us(self):
        contact_us = self.find_element(
            ToolBarPageLocator.LOCATOR_CLICK_BUTTON)
        contact_us.click()
