"""Простейший калькулятор v0.1
Реализуйте программу, которая спрашивала у пользователя, какую операцию он
хочет произвести над числами,а затем запрашивает два числа и выводит результат
Проверка деления на 0.
Пример
Выберите операцию:
    1. Сложение
    2. Вычитание
    3. Умножение
    4. Деление
Введите номер пункта меню:
4
Введите первое число:
10
Введите второе число:
3
Частное: 3, Остаток: 3
"""

possible_operations = '12345'


def calculations(selected_operation: str, val_1: int, val_2: int):
    if selected_operation == '1':
        res = val_1 + val_2
        print('Сумма равна ' + str(res))
    elif selected_operation == '2':
        res = val_1 - val_2
        print('Разность равна ' + str(res))
    elif selected_operation == '3':
        res = val_1 * val_2
        print('Умножение равно ' + str(res))
    elif selected_operation == '4':
        try:
            res = divmod(val_1, val_2)
            if res[1] != 0:
                print('Частное: ' + str(res[0]) + ', Остаток: ' + str(res[1]))
            else:
                print('Частное равно ' + str(res[0]))
            return res
        except ZeroDivisionError:
            print('Делить на ноль нельзя!')


while True:
    selected_operation = input('Выберите операцию:\n'
                               '1. Сложение\n'
                               '2. Вычитание\n'
                               '3. Умножение\n'
                               '4. Деление\n'
                               '5. Выход\n'
                               'Введите номер пункта меню: \n'
                               )
    if selected_operation in possible_operations:
        if selected_operation == '5':
            break
        try:
            val_1 = input('Введите первое число: ')
            val_2 = input('Введите второе число: ')
            val_1 = int(val_1)
            val_2 = int(val_2)
        except ValueError:
            print('Вы ввели некорректное значение!')
            break
        calculations(selected_operation, val_1, val_2)
        break
    else:
        print('Выберите доступную операцию!\n\n')
