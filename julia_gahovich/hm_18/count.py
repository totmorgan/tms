def counter(value):
    given_string = value
    cnt = 1
    x = 1
    compare_string = given_string[x:x + 1]
    result_string = ''

    for i in given_string:
        if i in compare_string:
            cnt += 1
        else:
            result_string += i
            if cnt != 1:
                result_string += str(cnt)
            cnt = 1
        x += 1
        compare_string = given_string[x:x + 1]

    return result_string
