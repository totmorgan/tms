"""Откройте страницу: https://ultimateqa.com/filling-out-forms/
!В данном задании работаем с ЛЕВОЙ формой обратной связи

тест1:
Заполните форму обратной связи
Нажмите на кнопку "Submit"
Проверьте наличие сообщения "Form filled out successfully"
тест2:
Заполните поле "Name"
Нажмите на кнопку "Submit"
Проверьте наличие сообщения об ошибке
тест3:
Заполните поле "Message"
Нажмите на кнопку "Submit"
Проверьте наличие сообщения об ошибке"""
import pytest


class TestFeedbackForm:

    @pytest.fixture()
    def feedback_form(self, browser):
        browser.get("https://ultimateqa.com/filling-out-forms/")

        self.input_name = browser.find_element_by_xpath(
            "//input[@id='et_pb_contact_name_0']")
        self.input_msg = browser.find_element_by_xpath(
            "//textarea[@id='et_pb_contact_message_0']")
        self.button = browser.find_element_by_xpath(
            "//button[@name='et_builder_submit_button']")

    def test_success_submit(self, browser, feedback_form):
        self.input_name.send_keys("Julia")
        self.input_msg.send_keys("Hello world")
        self.button.click()
        assert browser.find_element_by_xpath(
            "//div[@class='et-pb-contact-message']/p").text == \
            'Form filled out successfully', 'No success message'

    def test_empty_name(self, browser, feedback_form):
        self.input_name.send_keys("Julia")
        self.button.click()
        assert browser.find_element_by_xpath(
            "//div[@class='et-pb-contact-message']/p").text == \
            'Please, fill in the following fields:', 'No error message'

    def test_empty_msg(self, browser, feedback_form):
        self.input_msg.send_keys("Hello world")
        self.button.click()
        assert browser.find_element_by_xpath(
            "//div[@class='et-pb-contact-message']/p").text == \
            'Please, fill in the following fields:', 'No error message'
