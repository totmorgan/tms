"""Расширим возможности нашего калькулятора с прошлой домашней работы добавив
к уже реализованному функционалу еще и возможность того что: программа ожидает
от пользователя ввода математического выражения и правильно его трактует:

expressions for check:
20 / 2 / 2 + 3 * 2 + 2 / 2
20/ 2 / 2 + 3 * 2 + 2 / 0
 / 2 / 2 + 3 * 2 + 2 / 2
"""

possible_operations = '123456'


def plus(a, b):
    return a + b


def minus(a, b):
    return a - b


def multiple(a, b):
    return a * b


def division(a, b):
    return divmod(a, b)


def division_expr(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        print('Делить на ноль нельзя!')
        raise


def validate_expression(expr):
    if len(expr) == 0:
        return False

    if not (expr[0].isdigit() or expr[-1].isdigit()):
        return False

    for i in expr:
        if not(i.isdigit() or i in ' +-/*'):
            return False
        else:
            return False

    return True


def expression_parsing(func):
    def wrapper(val_3: str):
        member_list = ['']
        for character in val_3:
            if character.isdigit():
                if member_list[-1].isdigit():
                    member_list[-1] += character
                else:
                    member_list.append(character)
            elif character in '+-/*':
                member_list.append(character)

        return func(member_list[1:])
    return wrapper


@expression_parsing
def advanced_calculator(expr):
    x = complex_calculations(expr)
    return x


def complex_calculations(expression_parsing):
    new_member_list = expression_parsing
    if len(new_member_list) == 1:
        return new_member_list

    for i in new_member_list:
        if i in ['*', '/']:
            operator_index = new_member_list.index(i)
            first_digit = float(new_member_list[operator_index - 1])
            second_digit = float(new_member_list[operator_index + 1])
            if i == '*':
                result = multiple(first_digit, second_digit)
            else:
                result = division_expr(first_digit, second_digit)
            new_member_list[operator_index - 1] = float(result)
            del new_member_list[operator_index + 1]
            del new_member_list[operator_index]

    for i in new_member_list:
        if i in ['*', '/']:
            return complex_calculations(new_member_list)

    for i in new_member_list:
        if i in ['+', '-']:
            operator_index = new_member_list.index(i)
            first_digit = float(new_member_list[operator_index - 1])
            second_digit = float(new_member_list[operator_index + 1])
            if i == '+':
                result = plus(first_digit, second_digit)
            else:
                result = minus(first_digit, second_digit)
            new_member_list[operator_index - 1] = float(result)
            del new_member_list[operator_index + 1]
            del new_member_list[operator_index]

    for i in new_member_list:
        if i in ['+', '-']:
            return complex_calculations(new_member_list)

    return complex_calculations(new_member_list)


def simple_calculations(selected_operation: str, val_1: int, val_2: int):
    if selected_operation == '1':
        res = plus(val_1, val_2)
        print('Сумма равна ' + str(res))
    elif selected_operation == '2':
        res = minus(val_1, val_2)
        print('Разность равна ' + str(res))
    elif selected_operation == '3':
        res = multiple(val_1, val_2)
        print('Умножение равно ' + str(res))
    elif selected_operation == '4':
        try:
            res = division(val_1, val_2)
            if res[1] != 0:
                print('Частное: ' + str(res[0]) + ', Остаток: ' + str(res[1]))
            else:
                print('Частное равно ' + str(res[0]))
            return res
        except ZeroDivisionError:
            print('Делить на ноль нельзя!')


while True:
    selected_operation = input('Выберите операцию:\n'
                               '1. Сложение\n'
                               '2. Вычитание\n'
                               '3. Умножение\n'
                               '4. Деление\n'
                               '5. Ввести математическое выражение\n'
                               '6. Выход\n'
                               'Введите номер пункта меню: \n'
                               )
    if selected_operation in possible_operations:
        if selected_operation == '6':
            break
        elif selected_operation == '5':

            while True:
                val_3 = input('Введите выражение (целые числа и операторы): ')
                if validate_expression(val_3):
                    try:
                        result = advanced_calculator(val_3)
                        print(f'Результат:  {result[0]}')
                        break
                    except Exception:
                        pass
                else:
                    print('Вы ввели некорректное значение!')
        else:
            try:
                val_1 = input('Введите первое число: ')
                val_2 = input('Введите второе число: ')
                val_1 = int(val_1)
                val_2 = int(val_2)
            except ValueError:
                print('Вы ввели некорректное значение!')
                break
            simple_calculations(selected_operation, val_1, val_2)
        break
    else:
        print('Выберите доступную операцию!\n\n')
