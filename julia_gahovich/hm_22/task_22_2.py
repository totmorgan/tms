"""работа с xml файлом
Разработайте поиск книги в библиотеке по ее автору
(часть имени)/цене/заголовку/описанию.

Файл: library.xml
"""

import xml.etree.ElementTree as Et


def parse_xml(root, option, value):
    for child in root:
        for book in child:
            if book.tag == option and value in book.text:
                print(child.tag, child.attrib)


if __name__ == "__main__":
    tree = Et.parse('library.xml')
    root = tree.getroot()


parse_xml(root, 'author', 'Matthew')
parse_xml(root, 'price', '5.95')
parse_xml(root, 'title', 'Maeve')
parse_xml(root, 'description', 'Even bad code can function.')
