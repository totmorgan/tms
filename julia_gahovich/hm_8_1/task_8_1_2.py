"""Реализуйте декоратор caching, который в течение X последних секунд с
момента последнего вызова (в нашему случае 3) будет запоминать вычесленное
ранее значение, и возвращать его вместо повторного вызова функции.
Если время таймаута истекло между вызовами функции, значение должно быть
вычислено заново и еще раз закешировано на X секунд."""

from time import time, sleep


def caching(filter):
    def wrapper(func):
        cache = {}

        def inner(x):
            now_time = time()
            if x in cache.keys():
                if now_time - cache[x] > filter:
                    result = func(x)
                    cache[result] = now_time
                    print(cache)
                    return cache
                else:
                    print(cache)
                    return cache.values()
            else:
                result = func(x)
                cache[result] = now_time
                print(cache)
                return cache.values()

        return inner

    return wrapper


@caching(3)
def func(x):
    return x


x = func(2)
# sleep(4)
x = func(3)
sleep(2)
x = func(2)
