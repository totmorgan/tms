"""Frames
Открыть iFrame
Проверить, что текст внутри параграфа равен “Your content goes here.”
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_frame(browser):
    browser.get("http://the-internet.herokuapp.com/frames")
    browser.find_element_by_xpath('//div[@class="example"]/ul/li[2]/a').click()
    WebDriverWait(browser, 10).until(
        EC.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe")))

    assert browser.find_element_by_xpath(
        '//*[@id="tinymce"]/p').text == "Your content goes here."
