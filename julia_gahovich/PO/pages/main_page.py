from pages.base_page import BasePage
from locator.main_page_locator import MainPageLocator


class MainPage(BasePage):

    def open_contact_us_page(self):
        contact_us_link = self.find_element(
            MainPageLocator.LOCATOR_CONTACT_US_FORM)

        contact_us_link.click()

    def open_cart_page(self):
        cart_link = self.find_element(
            MainPageLocator.LOCATOR_CART)

        cart_link.click()

    def add_product_to_cart(self):
        product_link = self.find_element(
            MainPageLocator.LOCATOR_PRODUCT_IMAGE)
        product_link.click()
        add_to_cart = self.find_element(
            MainPageLocator.LOCATOR_SUBMIT_BUTTON)
        add_to_cart.click()

    def layer_cart(self):
        cross_button = self.find_element(
            MainPageLocator.LOCATOR_CROSS_BUTTON)

        cross_button.click()
