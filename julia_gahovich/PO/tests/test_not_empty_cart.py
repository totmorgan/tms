"""
2. Положить в корзину товар и проверить, что там отображаются вещи.
Оформить как два разных теста в стиле PO"""

from pages.main_page import MainPage
from pages.cart_page import CartPage
import time


def test_not_empty_cart(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.add_product_to_cart()
    time.sleep(1)
    main_page.layer_cart()
    main_page.open_cart_page()
    cart_page = CartPage(browser)
    cart_page.product_counter_is_present()
    cart_page.product_details_is_present()
    cart_page.checkout_button_is_present()
