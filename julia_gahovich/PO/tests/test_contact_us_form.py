"""Тест сценарий:
1. Открыть главную страницу
2. Нажать на Contact us
3. Заполнить форму
4. Нажать на кнопку send
5. Проверить, что отобразилось надпись
"Your message has been successfully sent to our team."
Тест нужно реализовать в стиле PO
Создать page для верхнего общего меню"""

from pages.main_page import MainPage
from pages.contact_us_page import ContactUsPage


def test_success_contact_us_submit(browser, contact_us_config_data):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_contact_us_page()
    contact_us_page = ContactUsPage(browser)
    contact_us_page.should_be_contact_us_page()
    contact_us_page.contact_us_form_is_present()
    contact_us_page.contact_us_text_is_present()
    subj_heading, email, order_ref, message = contact_us_config_data
    contact_us_page.contact_us_filling(subj_heading, email, order_ref, message)
    contact_us_page.contact_us_success_msg_is_present()
