from selenium import webdriver
import pytest
import os.path
import json


def load_config(file_path):
    config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                               file_path)
    with open(config_path) as f:
        target = json.load(f)
    return target


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get('http://automationpractice.com/index.php')
    yield driver
    driver.quit()


@pytest.fixture()
def contact_us_config_data():
    contact_us_data = load_config("resources/data_for_contact_us.json")
    return contact_us_data["subj_heading"], contact_us_data['email'],\
        contact_us_data['order_ref'], contact_us_data['message']
