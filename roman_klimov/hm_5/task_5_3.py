i = 0
while i < 100:
    i += 1
    if i % 5 == 0 and i % 3 == 0:
        print('fuzzbuzz')
    else:
        if i % 3 == 0:
            print('fuzz')
        else:
            if i % 5 == 0:
                print('buzz')
            else:
                print(i)
