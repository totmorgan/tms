from time import time, sleep


def func_decoration(func):

    def wrapper(a, b):
        start = time()
        print('Func started')
        print(func(a, b))
        sleep(1)
        print('Func finish')
        delta_time = time() - start
        print(f'Func worked: {delta_time} sec')
    return wrapper


@func_decoration
def func_result(a, b):
    return a ** b


func_result(10, 16)
