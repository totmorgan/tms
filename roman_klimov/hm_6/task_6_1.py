enter = input('Enter your card:')


def card(validate: int):
    i = str(validate)
    # Первичная проверка ввода карты
    if isinstance(validate, int):
        return print('False')
    # Проверка соответствия номера алгоритму Луна n16
    if len(i) == 16:
        sum = 0
        sum += int(i[15]) + int(i[13]) + int(i[11]) + int(i[9])\
            + int(i[7]) + int(i[5]) + int(i[3]) + int(i[1])
        if int(i[14]) * 2 > 9:
            sum += int(i[14]) * 2 - 9
        else:
            sum += int(i[14]) * 2
        if int(i[12]) * 2 > 9:
            sum += int(i[12]) * 2 - 9
        else:
            sum += int(i[12]) * 2
        if int(i[10]) * 2 > 9:
            sum += int(i[10]) * 2 - 9
        else:
            sum += int(i[10]) * 2
        if int(i[8]) * 2 > 9:
            sum += int(i[8]) * 2 - 9
        else:
            sum += int(i[8]) * 2
        if int(i[6]) * 2 > 9:
            sum += int(i[6]) * 2 - 9
        else:
            sum += int(i[6]) * 2
        if int(i[4]) * 2 > 9:
            sum += int(i[4]) * 2 - 9
        else:
            sum += int(i[4]) * 2
        if int(i[2]) * 2 > 9:
            sum += int(i[2]) * 2 - 9
        else:
            sum += int(i[2]) * 2
        if int(i[0]) * 2 > 9:
            sum += int(i[0]) * 2 - 9
        else:
            sum += int(i[0]) * 2

        # Проверка кратности суммы
        if sum % 10 != 0:
            return print(False)

    # Проверка соответствия номера алгоритму Луна n15
    if len(i) == 15:
        sum = 0
        sum += int(i[14]) + int(i[12]) + int(i[10]) + int(i[8]) \
            + int(i[6]) + int(i[4]) + int(i[2]) + int(i[0])
        if int(i[13]) * 2 > 9:
            sum += int(i[13]) * 2 - 9
        else:
            sum += int(i[13]) * 2
        if int(i[11]) * 2 > 9:
            sum += int(i[11]) * 2 - 9
        else:
            sum += int(i[11]) * 2
        if int(i[9]) * 2 > 9:
            sum += int(i[9]) * 2 - 9
        else:
            sum += int(i[9]) * 2
        if int(i[7]) * 2 > 9:
            sum += int(i[7]) * 2 - 9
        else:
            sum += int(i[7]) * 2
        if int(i[5]) * 2 > 9:
            sum += int(i[5]) * 2 - 9
        else:
            sum += int(i[5]) * 2
        if int(i[3]) * 2 > 9:
            sum += int(i[3]) * 2 - 9
        else:
            sum += int(i[3]) * 2
        if int(i[1]) * 2 > 9:
            sum += int(i[1]) * 2 - 9
        else:
            sum += int(i[1]) * 2

        # Проверка кратности суммы
        if sum % 10 != 0:
            return print(False)

    if len(i) < 15 or len(i) > 16:
        print('Введены не все символы')
    return print('True')


card(enter)
