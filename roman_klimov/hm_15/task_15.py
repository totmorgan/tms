from selenium import webdriver
from selenium.webdriver.common.by import By
import pytest


@pytest.fixture(scope='module')
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_homework_15_1(browser):

    browser.get('https://ultimateqa.com/complicated-page/')
    button_class_name = browser.find_element(
        By.CLASS_NAME, 'et_pb_button.et_pb_button_3.et_pb_bg_layout_light')
    button_class_name.click()
    butoon_xpath = browser.find_element(
        By.XPATH, '//a[@class="et_pb_button et_pb_button_3'
                  ' et_pb_bg_layout_light"]')
    butoon_xpath.click()
    button_css = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_button.et_pb_button_3.et_pb_bg_layout_light')
    button_css.click()


def test_homework_15_2(browser):

    browser.get('https://ultimateqa.com/filling-out-forms/')
    field_message = browser.find_element(
        By.NAME, 'et_pb_contact_message_0')
    field_message.click()
    field_message.send_keys('Hello! I have a problem.'
                            ' I do not see inscription: '
                            ' "Form filled out successfully"')
    button_submit = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_contact_submit.et_pb_button:nth-child(1)')
    button_submit.click()
    inscription_homework_1 = browser.find_element(
        By.CSS_SELECTOR, '.et-pb-contact-message > p').text
    assert 'Please, fill in the following fields' in inscription_homework_1,\
           'Тебует заполнения формы Имя'


def test_homework_15_3(browser):

    browser.get('https://ultimateqa.com/filling-out-forms/')
    field_message = browser.find_element(By.NAME, 'et_pb_contact_message_0')
    field_message.click()
    field_message.send_keys('any message')
    button_submit = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_contact_submit.et_pb_button:nth-child(1)')
    button_submit.click()
    field_name = browser.find_element(
        By.NAME, 'et_pb_contact_name_0')
    field_name.click()
    field_name.send_keys('John')
    button_submit.click()
    browser.implicitly_wait(3)
    inscription_error = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_contact_error_text').text
    assert 'Please refresh the page and try again' in inscription_error,\
           'Тебует повторить после обновления страницы'


def test_homework_15_4(browser):

    browser.get('https://ultimateqa.com/filling-out-forms/')
    field_message = browser.find_element(By.NAME, 'et_pb_contact_message_0')
    field_message.click()
    field_message.send_keys('any message')
    button_submit = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_contact_submit.et_pb_button:nth-child(1)')
    button_submit.click()
    field_name = browser.find_element(By.NAME, 'et_pb_contact_name_0')
    field_name.click()
    field_name.send_keys('John')
    button_submit.click()
    field_message = browser.find_element(By.NAME, 'et_pb_contact_message_0')
    field_message.click()
    field_message.send_keys('any message number 2')
    button_submit.click()
    browser.implicitly_wait(3)
    inscription_error = browser.find_element(
        By.CSS_SELECTOR, '.et_pb_contact_error_text').text
    assert 'Please refresh the page and try again' in inscription_error,\
           'Тебует повторить после обновления страницы'
