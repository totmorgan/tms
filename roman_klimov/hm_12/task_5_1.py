# Игра быки и коровы


def enter_number():
    x = list(input('Введите четырехзначное число'
                   ' с неповторяющимися цифрами:'))
    return x


def cows_and_bools(number):
    x_list = ['3', '2', '1', '9']

    entered_number_set = set(number)
    cows = 0
    bools = 0

    if len(entered_number_set) < 4 or len(entered_number_set) > 4:
        return ('Необходимо ввести четырехзначное число,'
                ' либо числа повторяются')
    else:
        for i in range(len(number)):
            if number[i] == x_list[i]:
                bools += 1

    if number[0] == x_list[1] or number[0] == x_list[2]\
            or number[0] == x_list[3]:
        cows += 1
    if number[1] == x_list[0] or number[1] == x_list[2]\
            or number[1] == x_list[3]:
        cows += 1
    if number[2] == x_list[0] or number[2] == x_list[1]\
            or number[2] == x_list[3]:
        cows += 1
    if number[3] == x_list[0] or number[3] == x_list[1]\
            or number[3] == x_list[2]:
        cows += 1

    return (f'быков: {bools}, коров: {cows}')


if __name__ == '__main__':
    print(cows_and_bools(enter_number()))
