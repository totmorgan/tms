from collections import Counter


def input_line():
    x = list(input('Введите строку для подсчета cимволов:'))
    return x


def counter_symbols(y):
    b = Counter(y).most_common(len(y))
    a = []
    for i in b:
        for k in i:
            a.append(k)
    return a


if __name__ == '__main__':
    print(counter_symbols(input_line()))
