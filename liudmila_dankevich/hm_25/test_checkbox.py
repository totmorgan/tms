# a.Dynamic Controls
# i.Найти чекбокс*
# ii.Нажать на кнопку*
# iii.Дождаться надписи “It’s gone”
# iv.Проверить, что чекбокса нет
# v.Найти инпут
# vi.Проверить, что он disabled
# vii.Нажать на кнопку
# viii.Дождаться надписи “It's enabled!”
# Проверить, что инпут enabled


import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_input_checkbox(browser):
    browser.get('http://the-internet.herokuapp.com/dynamic_controls')
    radio_button_checkbox = browser.find_element(
        By.XPATH, '//input[@type="checkbox"]'
    )
    radio_button_checkbox.click()
    time.sleep(3)
    remove_button = browser.find_element(
        By.XPATH, "//form[@id='checkbox-example']/""button"
    )
    remove_button.click()
    text_gone = WebDriverWait(browser, 20).until(
        EC.visibility_of_element_located((
            By.XPATH, '//*[@id="message"]'
        )))
    assert text_gone.text == "It's gone!"
    file_input_text = browser.find_element(
        By.XPATH, '//*[@id="input-example"]/input'
    )
    file_input_text.click()
    assert file_input_text.get_attribute(
        'disabled'), 'Field is enable'
    enable_button = browser.find_element(
        By.XPATH, "//*[@id='input-example']""/button"
    )
    enable_button.click()
    text_enabled = WebDriverWait(browser, 20).until(
        EC.visibility_of_element_located(
            (By.XPATH, '//form[@id="input-example"]//p'))
    )
    assert text_enabled.text == "It's enabled!"
    browser.close()
