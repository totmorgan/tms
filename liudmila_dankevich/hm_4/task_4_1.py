# Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" =>
# ["I", "love", "arrays", "they", "are",
# "my", "favorite"]

str_1 = "Robin Singh"
mas_1 = str_1 .split()
print(mas_1)

str_2 = "I love arrays they are my favorite"
mas_2 = str_2 . split()
print(mas_2)
