from pages.main_page import MainPage
from pages.cart_page import CartPage


def test_open_cart_page(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_cart_page()
    cart_page = CartPage(browser)
    cart_page.empty_cart()
