from selenium.webdriver.common.by import By


class ShortSleeveshirtsLocator:

    LOCATOR_ADD_TO_CART = (By.XPATH,
                           '//p[@id="add_to_cart"]/button'
                           )

    LOCATOR_CART = (By.XPATH,
                    '//div[@class="shopping_cart"]'
                    '//a[@title="View my shopping cart"]')

    # Данный локатор не находит, не могу понять почему
    LOCATOR_PROCEED_TO_CHECKOUT = (
        By.CLASS_NAME, 'btn btn-default button button-medium'
    )
