from pages.base_page import BasePage
from locator.main_page_locator import MainPageLocator


class MainPage(BasePage):
    # Находим кнопку log in и нажимаем её.

    def open_login_page(self):
        sing_in_button = self.find_element(
            MainPageLocator.LOCATOR_SING_IN_BUTTON
        )
        sing_in_button.click()

    # Находим кнопку cart и нажимаем её.

    def open_cart_page(self):
        cart_button = self.find_element(
            MainPageLocator.LOCATOR_CART_BUTTON)
        cart_button.click()

    # Находим кнопку каталога women и нажимаем её.

    def open_women_catalog_page(self):
        catalog_button = self.find_element(
            MainPageLocator.LOCATOR_WOMEN_BUTTON)
        catalog_button.click()
