# Задание 2
# 1.Открыть сайт http://demo.guru99.com/test/newtours/register.php
# 2.Заполнить все поля
# 3.Нажать кнопку Submit
# 4.Проверить, что отображается правильно имя и фамилия
# Подсказка xpath ".//tr//table//font[3]”
# 5.Проверить, что отображается правильно username
# Подсказка xpath ".//tr//table//font[5]”

import time


def test_input_field(browser):
    browser.get("http://demo.guru99.com/test/newtours/register.php")
    time.sleep(1)
    input_filed_First_Name = browser.find_element_by_xpath(
        '//input[@name="firstName"]')
    input_filed_First_Name.send_keys('liudmila')
    time.sleep(1)
    input_filed_Last_Name = browser.find_element_by_xpath(
        '//input[@name="lastName"]')
    input_filed_Last_Name.send_keys('Dankevich')
    time.sleep(1)
    input_filed_Phone = browser.find_element_by_xpath(
        '//input[@name="phone"]')
    input_filed_Phone.send_keys('+375(33)884-55-11')
    time.sleep(1)
    input_filed_Email = browser.find_element_by_id('userName')
    input_filed_Email.send_keys('Dan@mail.ru')
    time.sleep(1)
    input_filed_Address = browser.find_element_by_xpath(
        '//input[@name="address1"]')
    input_filed_Address.send_keys('Pobediteley 7-14')
    time.sleep(1)
    input_filed_City = browser.find_element_by_xpath(
        '//input[@name="city"]')
    input_filed_City.send_keys('Minsk')
    time.sleep(1)
    input_filed_State = browser.find_element_by_xpath(
        '//input[@name="state"]')
    input_filed_State.send_keys('Minskay')
    time.sleep(1)
    input_filed_Postal_Code = browser.find_element_by_xpath(
        '//input[@name="postalCode"]')
    input_filed_Postal_Code.send_keys('2170069')
    time.sleep(1)
    input_filed_Country = browser.find_element_by_xpath(
        "//select[@name='country']/option[text()='BELARUS']")
    input_filed_Country.click()
    time.sleep(1)
    input_filed_User_Name = browser.find_element_by_xpath(
        '//input[@name="email"]')
    input_filed_User_Name.send_keys('SHOK')
    time.sleep(1)
    input_filed_Password = browser.find_element_by_xpath(
        '//input[@name="password"]')
    input_filed_Password.send_keys('654321')
    time.sleep(1)
    input_filed_Confirm_Password = browser.find_element_by_xpath(
        '//input[@name="confirmPassword"]')
    input_filed_Confirm_Password.send_keys('654321')
    time.sleep(1)
    button_Submit = browser.find_element_by_xpath(
        '//input[@type="submit"]')
    button_Submit.click()
    time.sleep(1)
    value_one = browser.find_element_by_xpath(
        "//b[contains(text(),'Dear')]")
    value_two = browser.find_element_by_xpath(
        "//b[contains(text(),'Your user name')]")

    assert 'Dear liudmila Dankevich' == value_one.text
    assert 'Note: Your user name is SHOK' == value_two.text
    # не срабатывает, не могу разобраться почему
