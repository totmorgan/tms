# Задание 1:
# Залогинется созданным пользователем:
# Открыйть сайт http://thedemosite.co.uk/login.php
# 2. Ввести имя в поле username
# 3. Ввести пароль в поле password
# 4. Нажать на кнопку Test Login
# 5. Проверить, что Successful Login отображаются
import time


def add_user(browser):
    browser.get("http://thedemosite.co.uk/addauser.php")
    input_name = browser.find_element_by_xpath("//input[@name='username']")
    input_name.send_keys("liudmila")
    time.sleep(1)
    input_pswd = browser.find_element_by_xpath("//input[@name='password']")
    input_pswd.send_keys("123456")
    time.sleep(1)
    input_button = browser.find_element_by_xpath("//input[@type='button']")
    input_button.click()
    time.sleep(1)


def test_input_field(browser):
    browser.get("http://thedemosite.co.uk/login.php")
    time.sleep(1)
    input_filed = browser.find_element_by_xpath(
        '//input[@name="username"]')
    input_filed.send_keys('liudmila')
    time.sleep(1)
    input_filed_one = browser.find_element_by_xpath(
        '//input[@type="password"]')
    input_filed_one.send_keys('123456')
    time.sleep(1)
    button_Test_Login = browser.find_element_by_xpath(
        '//input[@type="button"]')
    time.sleep(1)
    button_Test_Login.click()

    assert '//input[@name="username"]' == \
           input_filed.get_attribute('value')
    assert '//input[@type="password"]' == \
           input_filed_one.get_attribute('value')
