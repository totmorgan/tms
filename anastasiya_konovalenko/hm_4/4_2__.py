ab = [
    {'a': 1, 'b': 2, 'c': 3},
    {'c': 3, 'd': 4, 'e': 5}
]

c = {}
for my_dict in ab:
    for key, value in my_dict.items():
        c.setdefault(key, []).append(value)

print(c)
