from time import time, sleep

cache = []


def caching(timeout):

    def wrapper(func):

        def inner(x):
            time_1 = time()
            res = func(x)
            time_2 = time()
            delta_time = time_2 - time_1
            if (delta_time > timeout) and (x in cache):
                cache[0] = delta_time
                cache[1] = res
                return res, delta_time

            return print(func(x))
        return inner

    return wrapper


@caching(5)
def function(y):
    return y


z = function(3)
assert z is function(3)
sleep(4)
assert z is not function(3)
