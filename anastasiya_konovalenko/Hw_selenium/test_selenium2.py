from selenium.webdriver.common.by import By
import pytest
from selenium import webdriver
from selenium.webdriver.support.ui import Select


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_create(browser):
    first_name = 'Pika'
    last_name = 'Chu'
    phone = '457676893454'
    email = '1111@gmail.com'
    address = 'guanchzou'
    city = 'china'
    state = 'st'
    postal_code = '221002'
    user_name = 'pik'
    passwd = 'qwerty123'
    confirm_pass = 'qwerty123'
    browser.get('http://demo.guru99.com/test/newtours/register.php')
    first_name_br = browser.find_element(By.NAME, 'firstName')
    first_name_br.send_keys(first_name)
    last_name_br = browser.find_element(By.NAME, 'lastName')
    last_name_br.send_keys(last_name)
    phone_br = browser.find_element(By.NAME, 'phone')
    phone_br.send_keys(phone)
    email_br = browser.find_element(By.NAME, 'userName')
    email_br.send_keys(email)
    address_br = browser.find_element(By.NAME, 'address1')
    address_br.send_keys(address)
    city_br = browser.find_element(By.NAME, 'city')
    city_br.send_keys(city)
    state_br = browser.find_element(By.NAME, 'state')
    state_br.send_keys(state)
    postal_code_br = browser.find_element(By.NAME, 'postalCode')
    postal_code_br.send_keys(postal_code)
    select = Select(browser.find_element(By.NAME, 'country'))
    select.select_by_visible_text("ALBANIA")
    user_name_br = browser.find_element(By.NAME, 'email')
    user_name_br.send_keys(user_name)
    passwd_br = browser.find_element(By.NAME, 'password')
    passwd_br.send_keys(passwd)
    confirm_pass_br = browser.find_element(By.NAME, 'confirmPassword')
    confirm_pass_br.send_keys(confirm_pass)
    button = browser.find_element(By.NAME, 'submit')
    button.click()
    test_name = browser.find_element_by_xpath("//font[3]").text
    test_user = browser.find_element_by_xpath("//font[5]").text
    assert first_name and last_name in test_name
    assert user_name in test_user
