from selenium.webdriver.common.by import By
import pytest
from selenium import webdriver
from time import sleep


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_create_us(browser):
    user_name = 'fdgghgfhgf'
    passwd = 'dfgdfg'
    browser.get("http://thedemosite.co.uk/login.php")
    browser.implicitly_wait(5)
    user_name_branch = browser.find_element(By.NAME, 'username')
    user_name_branch.send_keys(user_name)
    passwd_branch = browser.find_element(By.NAME, 'password')
    passwd_branch.send_keys(passwd)
    s_button = browser.find_element(By.NAME, 'FormsButton2')
    sleep(5)
    s_button.click()
    link = '//b[text()="**Successful Login**"]'
    assert '**Successful Login**' == browser.find_element(By.XPATH, link).text
