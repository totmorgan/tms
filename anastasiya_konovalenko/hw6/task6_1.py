def luhn(code):
    even = 0
    assert isinstance(code, int), 'Ты меня за дурака не держи!'
    code = str(code)
    for elem in code[-2::-2]:
        if (2 * int(elem)) > 9:
            elem = (2 * int(elem)) - 9
        else:
            elem = 2 * int(elem)
        even += int(elem)
    print('Сумма чётных: ', even)
    uneven = sum(int(el) for el in code[-1::-2])
    print('Сумма нечётных: ', uneven)
    if (even + uneven) % 10 == 0:
        return True
    else:
        return False


print(luhn(38520000023237))
