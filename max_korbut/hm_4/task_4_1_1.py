"""Написать программу, которая выводит уникальное число"""
from collections import Counter

"""
Узнаем количество каждого элемента, найдем элемент в количестве 1,
он и будет уникальным
"""
list = [1, 5, 2, 9, 2, 9, 1]
count = Counter(list)
for key, value in count.items():
    if value == 1:
        print(f"Уникальный номер: {key}")
