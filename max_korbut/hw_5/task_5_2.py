"""Посчитать кол-во лайков"""

likes = ['Макс', 'Миша', 'Степа', 'Илья', 'Иван', 'Я']
len_likes = len(likes)
"""Возможность вывода на 2х языках: rus и eng"""
en = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
rus = 'абвгдежзийклмнопрстуфхцчшщъыьэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
en_names = []
rus_names = []
for name in likes:
    for i in name:
        if i in en:
            en_names.append(name)
        elif i in rus:
            rus_names.append(name)
"""Проходимся циклом по списку и выводим корректный вариант вывода"""
if likes:
    if len_likes == 1:
        if en_names:
            print(f"{likes[0]} likes this!")
        elif rus_names:
            print(f"{likes[0]} лайкнул это!")
    elif 1 < len_likes < 3:
        if en_names:
            print(f"{likes[0]} and {likes[1]} like this!")
        elif rus_names:
            print(f"{likes[0]} и {likes[1]} лайкнули это!")
    elif 2 < len_likes < 4:
        if en_names:
            print(f"{likes[0]}, {likes[1]} and {likes[2]} like this!")
        elif rus_names:
            print(f"{likes[0]}, {likes[1]} и {likes[2]} лайкнули это!")
    elif 3 < len_likes:
        if en_names:
            print(f"{likes[0]}, {likes[1]} and {len_likes - 2} "
                  f"others like this!")
        elif rus_names:
            print(f"{likes[0]}, {likes[1]} и еще {len_likes - 2} "
                  f"тела лайкнули это!")
else:
    print("No one likes this! / У вас нет ни одного лайка!")
