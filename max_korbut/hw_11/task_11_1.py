import requests


def get_requests(url, author_id):
    """Получение списка авторов и конкретного автора по его id"""

    server_response = requests.get(url)
    json_response = server_response.json()
    for i in json_response:
        print(f"First name - {i['firstName']}, second - {i['lastName']}")
    for i in json_response:
        if i['id'] == author_id:
            print(f"По id '{author_id}' был найден автор с именем - "
                  f"{i['firstName']} и фамилией - {i['lastName']}")


def post_requests_book(url):
    """Добавляем новую книгу"""

    request = requests.post(url, json={
        "id": 2334,
        "title": "Freedom",
        "description": "Rights are not gifts from government",
        "pageCount": 1312,
        "excerpt": "Rights are inalienable aspects of our humanity",
        "publishDate": "2020-08-09T18:12:54.1234Z"
    })
    print(request.status_code)


def post_requests_user(url):
    """Добавляем нового пользователя"""

    request = requests.post(url, json={
        "id": 1312,
        "userName": "Max",
        "password": "1111"
    })
    print(request.status_code)


def put_requests(url):
    """Обновляем данные для книги под номером 10"""

    request = requests.put(url, json={
        "id": 1,
        "title": "changed title",
        "description": "changed description",
        "pageCount": 111,
        "excerpt": "changed excerpt",
        "publishDate": "2022-01-30T11:24:36.781Z"
    })
    print(request.status_code)


def delete_requests(url):
    """Удаление пользователя под номером 4"""

    request = requests.delete(url)
    print(request.status_code)


if __name__ == "__main__":
    url_for_get = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
    url_for_post_book = "https://fakerestapi.azurewebsites.net/api/v1/Books"
    url_for_post_user = "https://fakerestapi.azurewebsites.net/api/v1/Users"
    url_for_put = "https://fakerestapi.azurewebsites.net/api/v1/Books/1"
    url_for_delete = "https://fakerestapi.azurewebsites.net/api/v1/Users/4"

    get_requests(url_for_get, 3)
    post_requests_book(url_for_post_book)
    post_requests_user(url_for_post_user)
    put_requests(url_for_put)
    delete_requests(url_for_delete)
