import yaml
import json


def print_order_number(info):
    """выводит номер заказа"""

    order_number = info['invoice']
    print(f"Номер заказа - {order_number}")


def print_address(info):
    """Выводит адрес отправки"""

    city = info['bill-to']['address']['city']
    state = info['bill-to']['address']['state']
    print(f"Посылка отправлена из города {city}, штат {state}")


def print_info(info):
    """выводит описание посылки, ее стоимость и кол-во"""

    for i in range(len(info['product'])):
        description = info['product'][i]['description']
        price = info['product'][i]['price']
        quantity = info['product'][i]['quantity']
        print(f"{description} в количестве {quantity} по цене {price} за шт")


def convert_in_json(info):
    """Конвертирует из yaml в json"""

    with open('convert.json', 'w') as json_file:
        json.dump(str(info), json_file)


if __name__ == "__main__":
    with open('order.yaml') as yaml_file:
        order_info = yaml.safe_load(yaml_file)

    print_order_number(order_info)
    print_address(order_info)
    print_info(order_info)
    convert_in_json(order_info)
