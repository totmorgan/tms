import unittest
import random
from .bulls_cows import ranch


class TestRanchPositive(unittest.TestCase):

    def setUp(self):
        print('Start')
        self.number = random.sample('0123456789', 4)

    def tearDown(self):
        print('End')

    def test_positive_1(self):
        print('Positive test 1')
        self.assertEqual(ranch(self.number, self.number[::-1]), '0 bull '
                                                                'and 4 cows!')

    def test_positive_2(self):
        print('Positive test 2')
        self.assertEqual(ranch(self.number, self.number), f'Вы отгадали число,'
                                                          f' это было '
                                                          f'{self.number}')

    def test_positive_3(self):
        print('Positive test 3')
        self.assertEqual(ranch('1234', '1235'), '3 bulls and 0 cow!')

    def test_positive_4(self):
        print('Positive test 4')
        self.assertEqual(ranch('1234', '1562'), '1 bull and 1 cow!')

    def test_positive_5(self):
        print('Positive test 5')
        self.assertEqual(ranch('1234', '3456'), '0 bull and 2 cows!')

    def test_positive_6(self):
        print('Positive test 6')
        self.assertEqual(ranch('1234', '1243'), '2 bulls and 2 cows!')


@unittest.expectedFailure
class TestRanchNegative(unittest.TestCase):

    def setUp(self):
        print('Start')
        self.number = random.sample('0123456789', 4)

    def tearDown(self):
        print('End')

    def test_negative_1(self):
        print('Negative test 1')
        self.assertEqual(ranch(self.number, ''), 'Размер введенного числа не '
                                                 'равен 4')

    def test_negative_2(self):
        print('Negative test 2')
        self.assertEqual(ranch(self.number, '....'), 'Вы ввели не число')

    def test_negative_3(self):
        print('Negative test 3')
        self.assertEqual(ranch(self.number, 'qwdq'), 'Вы ввели не число')

    def test_negative_4(self):
        print('Negative test 4')
        self.assertEqual(ranch('qwer', '1234'), 'Загаданное значение не '
                                                'является числом')

    def test_negative_5(self):
        print('Negative test 5')
        self.assertEqual(ranch(self.number, '1111'), 'Вы передали число с '
                                                     'одинаковыми цифрами')
