import random


def ranch(hidden_number, number):
    while True:
        if hidden_number != number:
            """Узнаем одинаковые номера"""
            identical_numbers = []
            for i in number:
                if i in hidden_number:
                    identical_numbers.append(i)
            """Узнаем кол-во быков и коров"""
            bulls = []
            cows = []
            for i in identical_numbers:
                index_hidden_number = hidden_number.index(i)
                index_number = number.index(i)
                if index_hidden_number == index_number:
                    bulls.append(i)
                else:
                    cows.append(i)
            """Корректно выводим кол-во коров и быков игроку"""
            if len(bulls) < 2:
                if len(cows) < 2:
                    return f"{len(bulls)} bull and {len(cows)} cow!"
                elif len(cows) >= 2:
                    return f"{len(bulls)} bull and {len(cows)} cows!"
            elif len(bulls) >= 2:
                if len(cows) < 2:
                    return f"{len(bulls)} bulls and {len(cows)} cow!"
                elif len(cows) >= 2:
                    return f"{len(bulls)} bulls and {len(cows)} cows!"
            break
        else:
            return f"Вы отгадали число, это было {hidden_number}"


if __name__ == "__main__":
    random_number = random.sample('0123456789', 4)
    random_number = ''.join(random_number)
    print(random_number)
    try_number = str(input("Попытайтесь отгадать число: "))
    print(ranch(random_number, try_number))
