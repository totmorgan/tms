"""Лексикографическое возрастание"""


def decorate(func):

    def wrapper(arg):
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                        5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                        10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                        14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                        17: 'seventeen', 18: 'eighteen', 19: 'nineteen',
                        }
        names = []
        new_number_list = []
        arg = arg.split()
        for i in arg:
            for key, value in number_names.items():
                if int(i) == key:
                    names.append(value)
        names.sort()
        for name in names:
            for key, value in number_names.items():
                if name == value:
                    new_number_list.append(str(key))
        new_number_tuple = ' '.join(new_number_list)
        return func(new_number_tuple)

    return wrapper


@decorate
def sort_numbers(str):
    print(str)


sort_numbers('1 2 3')
