from pages.cart_page import CartPage
from pages.search_page import SearchPage


def test_empty_cart(browser):
    cart_page = CartPage(browser)
    cart_page.open_cart_page()
    cart_page.should_be_cart_page()
    cart_page.is_cart_empty()


def test_add_something_to_cart_and_check(browser):
    product_name = "Faded Short Sleeve T-shirts"
    search_page = SearchPage(browser)
    search_page.search_something(product_name)
    search_page.go_to_product_page_in_market()
    search_page.add_product_to_cart()
    search_page.close_modal_window()
    cart_page = CartPage(browser)
    cart_page.open_cart_page()
    product_text = cart_page.product_name_at_cart()
    assert product_text.text == product_name
