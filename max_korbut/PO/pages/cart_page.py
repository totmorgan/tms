from pages.base_page import BasePage
from locators.cart_page_locator import CartPageLocator


class CartPage(BasePage):

    def open_cart_page(self):
        cart_button = self.find_element(CartPageLocator.LOCATOR_CART_BUTTON)
        cart_button.click()

    def should_be_cart_page(self):
        cart_text = self.find_element(CartPageLocator.LOCATOR_CART_PAGE_TEXT)
        assert cart_text.text == "Your shopping cart"

    def is_cart_empty(self):
        status_cart_text = self.find_element(
            CartPageLocator.LOCATOR_STATUS_CART_TEXT)
        assert status_cart_text.text == "Your shopping cart is empty."

    def product_name_at_cart(self):
        product_name = self.find_element(
            CartPageLocator.LOCATOR_PRODUCT_NAME_TEXT)
        return product_name
