"""
Бонусное задание 4. Свой шифр будет представлен на примере азбуки морзе
На вход в зашифрованном виде подается шифр, где каждая буква отделена пробелом
"""


def encode_morse(text: str, morse_code: dict):
    """Превращает введенный текст в шифр азбуки морзе"""

    encode_text = []
    for letter in text:
        for key, value in morse_code.items():
            if letter.upper() == key:
                encode_text.append(value)
    encode_text = ' '.join(encode_text)
    print(f"'{text}' на языке азбуки морзе выглядет так: {encode_text}")


def decode_morse(text: str, morse_code: dict):
    """Превращает код морзе в текст"""

    decode_text = []
    text = text.split()
    for symbol in text:
        for key, value in morse_code.items():
            if symbol == value:
                decode_text.append(key)
    decode_text = ' '.join(decode_text)
    print(f"Шифр '{' '.join(text)}' после расшифровки: {decode_text}")


def start():
    morse_code = {
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..--',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
    }
    text = input("Введите текст и я превращу его в шифр Морзе "
                 "или расшифрую его: ")
    print("Вы хотите"
          "\n1. Зашифровать текст\n2. Расшифровать шифр")
    type_operation = int(input("Введите номер операции: "))
    if type_operation == 1:
        encode_morse(text, morse_code)
    elif type_operation == 2:
        decode_morse(text, morse_code)


if __name__ == '__main__':
    start()
