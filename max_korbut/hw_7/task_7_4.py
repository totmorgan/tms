"""Посчитать кол-во символов в словах, кроме the"""


def len_word(sentence: str):
    """Считает кол-во символов в словах, кроме the"""

    sentence = sentence.split()
    for word in sentence:
        if word != 'the':
            len_w = len(word)
            yield len_w


generator = len_word(" thequick brown fox jumps over the lazy dog")
for i in generator:
    print(i)
