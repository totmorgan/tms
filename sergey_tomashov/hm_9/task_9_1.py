
class Book:
    def __init__(self, name, author, pages, isbn,
                 reserved=False, reserved_by=None):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserved = reserved
        self.reserved_by = reserved_by


class User:
    def __init__(self, name, books=None):
        if books is None:
            books = []
        self.name = name
        self.books = books

    def get_book(self, book):
        if not book.reserved:
            book.reserved = True
            self.books.append(book.isbn)
            print(f'You got {book.name} : {book.isbn}')

        else:
            print('Book is not available')

    def return_book(self, book):
        if book.isbn in self.books:
            book.reserved = False
            self.books.remove(book.isbn)
        else:
            print('You dont have this book')

    def reserve_book(self, book):
        if not book.reserved:
            book.reserved_by = self.name
            book.reserved = True
            print(f'{book.reserved_by}, you reserve {book.name}')
        elif book.isbn in self.books:
            print(f'You already have: {book.name} : {book.isbn}')
        else:
            print(f'{book.name} is reserved or owned by another User')


book_1 = Book('Чапаев и пустота', 'Пелевин', 180, 55566)
book_2 = Book('Empire V', 'Пелевин', 180, 557566)

alina = User('Alina')
sergey = User('Sergey')
sergey.get_book(book_1)
sergey.reserve_book(book_1)
alina.reserve_book(book_1)
print(sergey.books)
alina.get_book(book_1)
