from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, \
    ElementNotInteractableException


class TestForm():

    def test_check_box(self, browser):
        browser.get('http://the-internet.herokuapp.com/dynamic_controls')
        self.checkbox = browser.find_element(
            By.XPATH, '//div[@id="checkbox"]/input[@label="blah"]')
        self.button = browser.find_element(
            By.XPATH, '//form[@id="checkbox-example"]//button')
        self.button.click()
        WebDriverWait(browser, 10).until(
            EC.invisibility_of_element(self.checkbox))
        try:
            WebDriverWait(browser, 5).until(
                EC.text_to_be_present_in_element(
                    (By.XPATH, "//form[@id='checkbox-example']"
                               "//p[@id='message']"),
                    "It's gone!"))
        except TimeoutException:
            print("It's gone! - text not found")
            assert False

    def test_input(self, browser):
        browser.get('http://the-internet.herokuapp.com/dynamic_controls')
        self.input_field = browser.find_element(
            By.XPATH, "//form[@id='input-example']/input")
        assert self.input_field.get_attribute('disabled')
        try:
            self.input_field.send_keys('1')
        except ElementNotInteractableException:
            assert True
        else:
            print('Field can be filled with text')
            assert False
        browser.find_element(
            By.XPATH, '//form[@id="input-example"]/button').click()
        WebDriverWait(browser, 10).until(
            EC.text_to_be_present_in_element(
                (By.XPATH, "//form[@id='input-example']//p[@id='message']"),
                "It's enabled!"))
        WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable((
                By.XPATH, "//form[@id='input-example']/input")))
        self.input_field.send_keys('1')
