import json


class JsonStudents:

    def __init__(self, file):
        with open(file, 'r') as f:
            self.data = json.load(f)

    # Поиск ученика по имени(часть имени)
    def search_name(self, st_name):
        for i in self.data:
            if st_name in i['Name']:
                print(i)

    def search_param(self, param, value):
        for i in self.data:
            if i[param] == value:
                print(i)


a = JsonStudents('students.json')
a.search_param('Club', 'Chess')
