likes = ['Sergey', 'Bob', 'Sergey', 'Bob']
l_len = len(likes)

language = ''
en = 'abcdefghijklmnopqrstuvwxyz'
ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

''''
This validation can be used to check if
en_count > ru_count, than display en, and so on...

en_count = 0
ru_count = 0
for i in likes:
    for l in i.lower().strip():
        if l in en:
            en_count += 1
        elif l in ru:
            ru_count +=1
print(en_count, ru_count)
'''

if len(likes) == 0:
    language = ''
elif likes[0][-1].lower() in en:
    language = 'en'
elif likes[0][-1].lower() in ru:
    language = 'ru'

if l_len > 3:
    if language == 'ru':
        print(f'{likes[0]}, {likes[1]} и {l_len - 2} дргуим нравится это')
    elif language == 'en':
        print(f'{likes[0]}, {likes[1]} and {l_len - 2} others like it')
elif l_len == 3:
    if language == 'ru':
        print(f'{likes[0]}, {likes[1]} и {likes[2]} лайкнули это')
    elif language == 'en':
        print(f'{likes[0]}, {likes[1]} and {likes[2]} like this')
elif l_len == 2:
    if language == 'ru':
        print(f'{likes[0]} и {likes[1]} лайкнули это')
    elif language == 'en':
        print(f'{likes[0]} and {likes[1]} like this')
elif l_len == 1:
    if language == 'ru':
        print(f'{likes[0]} лайкнул/а это')
    elif language == 'en':
        print(f'{likes[0]} like this')
else:
    print("no one likes this")
