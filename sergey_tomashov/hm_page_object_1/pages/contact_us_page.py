from sergey_tomashov.hm_page_object_1.pages.base_page import \
    BasePage
from sergey_tomashov.hm_page_object_1.locators.contact_us_locators \
    import ContuctUsLocators
from selenium.webdriver.support.ui import Select
import random
from sergey_tomashov.hm_page_object_1.resources.helpers.generators \
    import generate_email, generate_order, generate_message


class ContuctUsPAge(BasePage, ContuctUsLocators):

    def select_subject(self):
        select = Select(self.find_element(self.LOCATOR_SUBJECT_HEADING))
        select.select_by_index(random.randrange(1, 2))

    def fill_in_email(self):
        email = generate_email()
        email_field = self.find_element(self.LOCATOR_EMAIL)
        email_field.send_keys(email)

    def fill_in_rand_order(self):
        field = self.find_element(self.LOCATOR_ORDER_REF)
        order = generate_order()
        field.send_keys(order)

    def fill_in_mes(self):
        mes = self.find_element(self.LOCATOR_MESSAGE)
        m = generate_message()
        mes.send_keys(m)

    def submit_contuct_us(self):
        submit = self.find_element(self.LOCATOR_SUBMIT_FORM)
        submit.click()

    def verify_form_sent(self):
        s_text = self.find_element(self.LOCATOR_SUCCESS)
        check_text = "Your message has been successfully sent to our team."
        assert s_text.text == check_text, f"{s_text.text} != check_text"
