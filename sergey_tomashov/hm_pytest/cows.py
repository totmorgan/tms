def cows(hidden_number, user_number):
    cow = 0
    bulls = 0
    no = 0

    if (user_number.isdigit() is False) or (len(user_number) != 4):
        return f'{user_number} input should be 4 non repeatable digits'

    if user_number == hidden_number:
        return f'Congrats! You {user_number} same as {hidden_number}'

    for i in user_number:
        if user_number.count(i) > 1:
            return 'Repeatable digits in input'

    for i in range(0, len(user_number)):
        if user_number[i] == hidden_number[i]:
            bulls += 1
        elif user_number[i] in hidden_number:
            cow += 1
        else:
            no += 1
    return f'Cows :{cow}, Bulls: {bulls}'


if __name__ == '__main__':
    print(cows('1234', '1234'))
