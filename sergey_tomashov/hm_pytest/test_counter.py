import pytest
from hm_pytest.counter_str import string_counter


@pytest.mark.regression
class TestCounter():
    string = 'abbcccdddd'

    def test_valid(self):
        assert string_counter(self.string) == 'ab2c3d4'

    def test_spaces(self):
        assert string_counter('    ') == ' 4'

    def test_empty(self):
        assert string_counter('') == ''

    def test_nums(self):
        assert string_counter('1222') == '123'

    def test_special(self):
        assert string_counter('!!@@') == '!2@2'
