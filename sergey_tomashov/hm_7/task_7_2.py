def reshape(ls: list, cells: int, cell_size: int) -> list:
    list_cells = [[] for i in range(cells)]
    gen_ls = (el for el in ls)
    for cell in list_cells:
        if len(cell) < cell_size:
            for i in gen_ls:
                cell.append(i)
                if len(cell) >= cell_size:
                    break
    return list_cells


print(reshape([1, 2, 3, 4, 5, 6, 7, 8, ], 2, 4))
