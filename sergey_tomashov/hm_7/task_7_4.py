sentence = " thequick brown fox jumps over the lazy dog"


def str_count(sentence: str):
    sentence = sentence.split()
    for i in sentence:
        if i != 'the':
            yield len(i)


def ls_with_count_of_words(sentence: str) -> list:
    result = []
    for i in str_count(sentence):
        result.append(i)
    return result


print(ls_with_count_of_words(sentence))
